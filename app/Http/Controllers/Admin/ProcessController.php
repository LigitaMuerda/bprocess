<?php

namespace App\Http\Controllers\Admin;

use App\LProcess;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProcessController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.processes.index',[
            'processes' => LProcess::paginate(10)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.processes.create',[
            'process'   => []
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $p = new LProcess;

        $f = $request->file('document');
        $f_name = $f->getFilename() . $f->hashName();
        $f->move(public_path() . '/files',$f_name);

        $p->stage = $request->input('stage', '');
        $p->begin = $request->input('begin', '01.01.1975');
        $p->end = $request->input('end', '31.12.9999');
        $p->responsive = $request->input('responsive', '');
        $p->document = $f_name;
        $p->execution = $request->input('execution', 'Не исполнено');
        $p->tooltip = $request->input('tooltip', '');
        $p->chc = $request->input('chc', 'red');

        $p->save();

        return redirect()->route('admin.process.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\LProcess  $lProcess
     * @return \Illuminate\Http\Response
     */
    public function show(LProcess $lProcess)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\LProcess  $lProcess
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,LProcess $lProcess)
    {
        $p_id = mbsplit('/',$request->path())[2];
        $p = LProcess::findOrFail($p_id);

        return view('admin.processes.edit',[
            'process'   => $p
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\LProcess  $lProcess
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LProcess $lProcess)
    {
        $p_id = mbsplit('/',$request->path())[2];

        $p = LProcess::findOrFail($p_id);

        $new_stage = $request->input('stage');
        $new_begin = $request->input('begin');
        $new_end = $request->input('end');
        $new_responsive = $request->input('responsive');
        $new_execution = $request->input('execution');
        $new_tooltip = $request->input('tooltip');
        $new_chc = $request->input('chc');

        if ($new_stage && $new_stage != $p->stage) $p->stage = $new_stage;
        if ($new_begin && $new_begin != $p->begin) $p->begin = $new_begin;
        if ($new_end && $new_end != $p->end) $p->end = $new_end;
        if ($new_responsive && $new_responsive != $p->responsive) $p->responsive = $new_responsive;
        if ($new_execution && $new_stage != $p->execution) $p->execution = $new_execution;
        if ($new_tooltip && $new_tooltip != $p->tooltip) $p->tooltip = $new_tooltip;
        if ($new_chc && $new_chc != $p->chc) $p->chc = $new_chc;

        if ($request->hasFile('document')) {
            $f = $request->file('document');
            $f_name = $f->getFilename() . $f->hashName();
            $f->move(public_path() . '/files',$f_name);

            $p->document = $f_name;
        }

        $p->save();

        return redirect()->route('admin.process.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\LProcess  $lProcess
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,LProcess $lProcess)
    {
        $p_id = mbsplit('/',$request->path())[2];
        $p = LProcess::findOrFail($p_id)->ForceDelete();

        return redirect()->route('admin.process.index');
    }
}
