<?php
namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class LProcess extends JsonResource {
    public function toArray($request)
    {
        $begin = explode('.', $this->begin);
        $end = explode('.', $this->end);

        $date_from = [
            'day' => $begin[0],
            'month' => $begin[1],
            'year' => $begin[2]
        ];

        $date_to = [
            'day' => $end[0],
            'month' => $end[1],
            'year' => $end[2]
        ];

        return [
            'id' => $this->id,
            'item' => $this->stage,
            'date_from' => $date_from,
            'date_to' => $date_to,
            'name' => $this->responsive,
            'file' => $this->document,
            'status' => $this->execution,
            'tooltip' => $this->tooltip,
            'colorHoverCalendar' => $this->chc,
            'is_active' => $this->is_active,
        ];
    }
}
