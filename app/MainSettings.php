<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MainSettings extends Model
{
    protected $table = 'main_settings';
    public $timestamps = false;
}
