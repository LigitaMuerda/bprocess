import VueRouter from 'vue-router';
import ProcessesByDay from './components/ProcessesByDay';

export default new VueRouter({
   routes: [
       {
           path: '/',
           component: ProcessesByDay
       }
   ],
    mode: 'history'
});
