@extends('layouts.app')

@section('content')
    <div class="container">

{{--        <a href="{{route('admin.process.create')}}" class="btn btn-primary pull-right"><i class="fa fa-plus-square-0"></i> Добавить процесс</a>--}}
        <div class="row">
            <div class="col-12">
                @forelse($settings as $setting)
                <h1 class="text-center">Настройки сайта</h1>
                <hr>
                <h2>Главный заголовок страницы</h2>
                <p>{{ $setting->main_title }}</p>
                <h2>Заголовок первой таблицы</h2>
                <p>{{ $setting->second_title }}</p>
                <h2>Заголовок второй таблицы</h2>
                <p>{{ $setting->third_title }}</p>
                <h2>Заголовок календаря</h2>
                <p>{{ $setting->calendar_title }}</p>
                <h2>Изображение в  шапке</h2>
                <img src="@if(isset($setting->main_image)) {{$setting->main_image}} @endif /img/header_bg.jfif" alt="" width="100%;">
                @endforelse
            </div>
        </div>
        {{--<table class="table table-striped">
            <thead>
            <th>ID</th>
            <th>Название</th>
            <th>Иполнитель</th>
            <th>Подсказка</th>
            <th>Дата начала</th>
            <th>Дата окончания</th>
            <th>Статус</th>
            <th>Цвет на календаре</th>
            <th>Файл</th>
            <th>Действия</th>
            </thead>
            <tbody>
            @forelse($processes as $process)
                <tr>
                    <td>{{$process->id}}</td>
                    <td>{{$process->stage}}</td>
                    <td>{{$process->responsive}}</td>
                    <td>{{$process->tooltip}}</td>
                    <td>{{$process->begin}}</td>
                    <td>{{$process->end}}</td>
                    <td>{{$process->execution}}</td>
                    <td>{{$process->chc}}</td>
                    <td>{{$process->document}}</td>
                    <td>
                        <form onsubmit="if(confirm('Удалить процесс?')){return true}else{return false}" action="{{route('admin.process.destroy', $process->id)}}" method="post">
                            <input type="hidden" name="_method" value="DELETE">
                            {{ csrf_field() }}
                            <a class="btn" href="{{route('admin.process.edit', $process->id)}}"><i class="fa fa-edit"></i></a>
                            <button type="submit" class="btn"><i class="fa fa-trash-o"></i></button>
                        </form>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="3" class="text-center">Данные отсутствуют</td>
                </tr>
            @endforelse
            </tbody>
            <tfoot>
            <tr>
                <td colspan="10">
                    <ul class="pagination pull-right">
                        {{$processes->links()}}
                    </ul>
                </td>
            </tr>
            </tfoot>
        </table>--}}
    </div>
@endsection
