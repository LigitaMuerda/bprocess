<label for="">Статус</label>
<select name="is_active" class="form-control">
    @if(isset($process->id))
        <option value="0" @if($process->is_active == 0 ) selected="" @endif>Не активна</option>
        <option value="0" @if($process->is_active == 1 ) selected="" @endif>Aктивна</option>
    @else
        <option value="0">Не активна</option>
        <option value="0">Aктивна</option>
    @endif
</select>

<label for="">Название процесса</label>
<input type="text" class="form-control" name="stage" maxlength="255"  placeholder="Название процесса" value="@if(isset($process->stage)) {{$process->stage}} @endif" required>

<label for="">Исполнитель</label>
<input type="text" class="form-control" name="responsive" maxlength="55"  placeholder="ФИО исполнителя" value="@if(isset($process->responsive)) {{$process->responsive}} @endif" required>

<label for="">Дата начала (например 10.10.2020)</label>
<input type="text" class="form-control" name="begin" maxlength="11"  placeholder="Дата начала" value="@if(isset($process->begin)) {{$process->begin}} @endif" required>

<label for="">Дата окончания(например 10.10.2020)</label>
<input type="text" class="form-control" name="end" maxlength="11" placeholder="Дата окончания" value="@if(isset($process->end)) {{$process->end}} @endif" required>

<label for="">Подсказка</label>
<input type="text" maxlength="255" class="form-control" name="tooltip" placeholder="Текст подсказки" value="@if(isset($process->tooltip)) {{$process->tooltip}} @endif" required>

<label for="">Статус исполнения</label>
<input type="text" class="form-control" maxlength="55"  name="execution" placeholder="Статус" value="@if(isset($process->execution)) {{$process->execution}} @endif" required>

<label for="">Файл</label>
<input type="file" class="form-control" name="document" placeholder="Название файла" value="@if(isset($process->document)) {{$process->document}} @endif">

<label for="">Цвет выделения на календаре</label>
<input type="text" class="form-control" name="chc" placeholder="red" value="@if(isset($process->chc)) {{$process->chc}} @endif">

<input type="submit" class="btn btn-primary" value="Сохранить">


