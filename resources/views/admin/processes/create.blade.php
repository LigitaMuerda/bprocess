@extends('layouts.app')

@section('content')
    <div class="container">
        <hr>
        <form class="form-horizontal" action="{{route('admin.process.store')}}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            @include('admin.processes.partials.form')
        </form>
    </div>
@endsection
