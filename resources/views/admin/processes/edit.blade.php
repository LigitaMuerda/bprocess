@extends('layouts.app')

@section('content')
<div class="container">
        <form class="form-horizontal" action="{{route('admin.process.update', $process)}}" method="post" enctype="multipart/form-data">
            <input type="hidden" name="_method" value="put">
            {{ csrf_field() }}
            @include('admin.processes.partials.form')
        </form>
</div>
@endsection
