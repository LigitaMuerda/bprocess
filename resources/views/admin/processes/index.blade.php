@extends('layouts.app')

@section('content')
    <div class="container">

        <a href="{{route('admin.process.create')}}" class="btn btn-primary pull-right"><i class="fa fa-plus-square-0"></i> Добавить процесс</a>
        <table class="table table-striped">
            <thead>
                <th>ID</th>
                <th>Название</th>
                <th>Иполнитель</th>
                <th>Подсказка</th>
                <th>Дата начала</th>
                <th>Дата окончания</th>
                <th>Статус</th>
                <th>Цвет на календаре</th>
                <th>Файл</th>
                <th>Действия</th>
            </thead>
            <tbody>
            @forelse($processes as $process)
                <tr>
                    <td>{{$process->id}}</td>
                    <td>{{$process->stage}}</td>
                    <td>{{$process->responsive}}</td>
                    <td>{{$process->tooltip}}</td>
                    <td>{{$process->begin}}</td>
                    <td>{{$process->end}}</td>
                    <td>{{$process->execution}}</td>
                    <td>{{$process->chc}}</td>
                    <td>{{$process->document}}</td>
                    <td>
                        <form onsubmit="if(confirm('Удалить процесс?')){return true}else{return false}" action="{{route('admin.process.destroy', $process->id)}}" method="post">
                            <input type="hidden" name="_method" value="DELETE">
                            {{ csrf_field() }}
                            <a class="btn" href="{{route('admin.process.edit', $process->id)}}"><i class="fa fa-edit"></i></a>
                            <button type="submit" class="btn"><i class="fa fa-trash-o"></i></button>
                        </form>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="3" class="text-center">Данные отсутствуют</td>
                </tr>
            @endforelse
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="10">
                        <ul class="pagination pull-right">
                            {{$processes->links()}}
                        </ul>
                    </td>
                </tr>
            </tfoot>
        </table>
    </div>
@endsection
