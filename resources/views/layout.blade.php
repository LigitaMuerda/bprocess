<!DOCTYPE html>
<html>
@include('layouts.head')
<body>
<div id="main">

    @yield('content')
</div>
@include('layouts.footer')
{{--<script type="text/javascript" src="js/custom.js"></script>--}}
</body>
</html>
