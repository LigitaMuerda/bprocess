<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\LUser;
use App\Http\Resources\LUser as LUserResource;
use App\LProcess;
use App\Http\Resources\LProcess as LProcessResource;

function str_ends_with($haystack, $needle)
{
    $length = strlen($needle);
    if ($length == 0) {
        return true;
    }

    return (substr($haystack, -$length) === $needle);
}

Route::get('/', function () {
    return view('welcome');
});

// Get all processes
Route::get('/lava/processes', function(Illuminate\Http\Request $request) {
    $p = [];

    foreach (LProcess::all() as $prcs) {
        array_push($p, (new LProcessResource($prcs))->toArray($request));
    }

    return json_encode($p);
});

// Get specific process
Route::get('/lava/processes/specific', function(Illuminate\Http\Request $request) {
    $result = [];
    foreach (LProcess::all() as $p) {
        if (str_ends_with($p->begin, $request->input('year')) || str_ends_with($p->end, $request->input('year')))
            array_push($result, (new LProcessResource($p))->toArray($request));
    }

    return json_encode($result);
});

// Update record
Route::post('/lava/processes/{id}', function(Illuminate\Http\Request $request, $id) {
    $p = LProcess::findOrFail($id);

    if (!session('auth', false))
        return json_encode(['success' => false, 'error' => 1]);
    else {
        $new_stage = $request->input('stage');
        $new_begin = $request->input('begin');
        $new_end = $request->input('end');
        $new_responsive = $request->input('responsive');
        $new_document = $request->input('document');
        $new_execution = $request->input('execution');

        if ($new_stage && $new_stage != $p->stage) $p->stage = $new_stage;
        if ($new_begin && $new_begin != $p->begin) $p->begin = $new_begin;
        if ($new_end && $new_end != $p->end) $p->end = $new_end;
        if ($new_responsive && $new_responsive != $p->responsive) $p->responsive = $new_responsive;
        if ($new_document && $new_document != $p->document) $p->document = $new_document;
        if ($new_execution && $new_stage != $p->execution) $p->execution = $new_execution;

        $p->save();
    }
});

Route::get('/lava/user', function(Illuminate\Http\Request $request) {
    $us = [];
    foreach (LUser::all() as $u)
        array_push((new LUserResource($u))->toArray($request));

    return json_encode($us);
});

Route::get('/lava/user/{id}', function($id) {
    return new LUserResource(LUser::findOrFail($id));
});

Route::get('/lava/user/me', function() {
    /*
    1 = User is not authenticated
    */

    if (session('auth', false)) {
        return new LUserResource(LUser::findOrFail(session('id')));
    } else {
        return json_encode(['success' => false, 'error' => 1]);
    }
});

Route::post('/lava/user/me/logout', function() {
    session(['auth' => false, 'id' => 0]);
});

Route::post('/lava/user/me/auth', function(Illuminate\Http\Request $request) {
    /*
    1 = User not found in database
    2 = Incorrect password
    */

    $users = LUser::where('email', $request->input('email'));
    if ($users->count() > 0) {
        $u = $users->first();

        if ($u->password === sha1($request->input('password') . '0JKQf0v12yDBUorvwHruiKhjVKDXJuQb'))
        {
            session(['auth' => true, 'id' => $u->id, 'email' => $u->email, 'name' => $u->name]);
            return json_encode(['success' => true]);
        } else
        {
            return json_encode(['success' => false, 'error' => 2]);
        }
    } else {
        return json_encode(['success' => false, 'error' => 1]);
    }
});

Route::post('/lava/user/me/register', function(Illuminate\Http\Request $request) {
    if (LUser::where('email', $request->input('email'))->count() > 0)
    {
        return '{"success":false,"error":0}'; // Email already exists
    }

    if (LUser::where('phone', $request->input('phone'))->count() > 0)
    {
        return '{"success":false,"error":1}'; // Phone already exists
    }

    $u = new LUser;
    $u->name = $request->input('name');
    $u->email = $request->input('email');
    $u->phone = $request->input('phone');
    $u->password = sha1($request->input('password') . '0JKQf0v12yDBUorvwHruiKhjVKDXJuQb');
    $u->save();

    return '{"success":true, "id":' . $u->id . '}';
});
